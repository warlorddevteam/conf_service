package conf

import entitys "bitbucket.org/renegatumsoulteame/parking_structs/entitys"

var AuthsConfs map[string]entitys.Conf

func init() {
	AuthsConfs = make(map[string]entitys.Conf)
	AuthsConfs["test"] = entitys.Conf{
		HTTPServerPort: "4001", HTTPServerHost: "localhost",
		RedisHost: "localhost", RedisPort: "6379",
		CocrouchDbHost: "localhost", CocrouchDbPort: "26257",
		CocrouchDbName: "parking", CocrouchDbUser: "root",
	}
	AuthsConfs["dev"] = entitys.Conf{
		HTTPServerPort: "4001", HTTPServerHost: "localhost",
		RedisHost: "localhost", RedisPort: "6379",
		CocrouchDbHost: "localhost", CocrouchDbPort: "26257",
		CocrouchDbName: "parking", CocrouchDbUser: "root",
	}
	AuthsConfs["prod"] = entitys.Conf{
		HTTPServerPort: "4001", HTTPServerHost: "localhost",
		RedisHost: "localhost", RedisPort: "6379",
		CocrouchDbHost: "localhost", CocrouchDbPort: "26257",
		CocrouchDbName: "parking", CocrouchDbUser: "root",
	}
}
