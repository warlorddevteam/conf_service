package conf

import (
	entitys "bitbucket.org/renegatumsoulteame/parking_structs/entitys"
)

func configForParking(mod string) entitys.Conf {
	conf := modeSwitcher(ParkingsConfs, mod)
	return conf
}

func configForAuth(mod string) entitys.Conf {
	conf := modeSwitcher(AuthsConfs, mod)
	return conf
}

func configForChecker(mod string) entitys.Conf {
	conf := modeSwitcher(CheckerConfs, mod)
	return conf
}

func modeSwitcher(confMap map[string]entitys.Conf, mod string) entitys.Conf {
	selectedConf := entitys.Conf{}
	switch mod {
	case "dev":
		selectedConf = confMap["dev"]
	case "prod":
		selectedConf = confMap["prod"]
	case "test":
		selectedConf = confMap["test"]
	}
	return selectedConf
}

func SwitcherAppName(appName string, mod string) entitys.Conf {
	conf := entitys.Conf{}
	switch appName {
	case "auth":
		conf = configForAuth(mod)
	case "parking":
		conf = configForParking(mod)
	case "checker":
		conf = configForChecker(mod)
	}
	return conf
}
