package conf

import entitys "bitbucket.org/renegatumsoulteame/parking_structs/entitys"

var CheckerConfs map[string]entitys.Conf

func init() {
	CheckerConfs = make(map[string]entitys.Conf)
	CheckerConfs["test"] = entitys.Conf{
		HTTPServerPort: "4002", HTTPServerHost: "localhost",
		RedisHost: "localhost", RedisPort: "6379",
	}
	CheckerConfs["dev"] = entitys.Conf{
		HTTPServerPort: "4002", HTTPServerHost: "localhost",
		RedisHost: "localhost", RedisPort: "6379",
	}
	CheckerConfs["prod"] = entitys.Conf{
		HTTPServerPort: "4002", HTTPServerHost: "localhost",
		RedisHost: "localhost", RedisPort: "6379",
	}
}
