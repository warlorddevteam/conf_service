package conf

import entitys "bitbucket.org/renegatumsoulteame/parking_structs/entitys"

var ParkingsConfs map[string]entitys.Conf

func init() {
	ParkingsConfs := make(map[string]entitys.Conf)
	ParkingsConfs["test"] = entitys.Conf{
		HTTPServerPort: "4003", HTTPServerHost: "localhost",
		CocrouchDbHost: "localhost", CocrouchDbPort: "26257",
		CocrouchDbName: "parking", CocrouchDbUser: "root",
	}
	ParkingsConfs["dev"] = entitys.Conf{
		HTTPServerPort: "4003", HTTPServerHost: "localhost",
		CocrouchDbHost: "localhost", CocrouchDbPort: "26257",
		CocrouchDbName: "parking", CocrouchDbUser: "root",
	}
	ParkingsConfs["prod"] = entitys.Conf{
		HTTPServerPort: "4003", HTTPServerHost: "localhost",
		CocrouchDbHost: "localhost", CocrouchDbPort: "26257",
		CocrouchDbName: "parking", CocrouchDbUser: "root",
	}
}
