package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"bitbucket.org/renegatumsoulteame/conf_service/conf"
)

func main() {
	sendConf := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var test_struct struct {
			AppName string `json:"app_name"`
			AppMod  string `json:"app_mod"`
		}
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			print(err)
		}
		err = json.Unmarshal(body, &test_struct)
		if err != nil {
			print(err)
		}
		resData, _ := json.Marshal(conf.SwitcherAppName(test_struct.AppName, test_struct.AppMod))
		w.Header().Set("content-type", "application/json")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Write(resData)
		log.Println("sended config to app:", test_struct.AppName)
	})
	log.Println("running on port:", 9999)
	err := http.ListenAndServe(":9999", sendConf)
	log.Fatal(err)
}
